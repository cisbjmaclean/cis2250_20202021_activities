package MultiThreading;

import MultiThreading.Threads.ConsoleThread;
import MultiThreading.Threads.GUIThread;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Control flow of Multithreading portion of the assignment
 * 
 * @author Ryan van Kroonenburg
 * @since 20201202
 */
public class Controller {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        try{
            ConsoleThread thread1 = new ConsoleThread();
            thread1.start();
            System.out.println("Console thread begins...");
            
            Thread thread2 = new Thread(new GUIThread());
            thread2.start();
            
            thread1.join();
            thread2.join();
            
            
        } catch(InterruptedException ie){
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ie);
        }

    }

}
