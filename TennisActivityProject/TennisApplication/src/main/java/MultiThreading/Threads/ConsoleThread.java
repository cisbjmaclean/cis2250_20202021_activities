package MultiThreading.Threads;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

/**
 * Class to represent a console based input form
 * @author Ryan van Kroonenburg
 * @since 20201202
 */
public class ConsoleThread extends Thread {

    String testString;
    
    //create file if it does not exist.
    public void createFile() {

        while (!"exit".equals(testString)) {
            Scanner input = new Scanner(System.in);
            System.out.println("Enter text to write to file. Type 'exit' to exit.");
            testString = input.nextLine();
        }

        try {
            File file = new File("C:\\cis\\cis2232\\RvkTestFile.txt");
            if (file.createNewFile()) {
                System.out.println("File created: " + file.getName());
            } else {
                System.out.println("File already exists.");
            }
        } catch (IOException ex) {
            System.out.println("Error");
            ex.printStackTrace();
        }
    }

    //write input to file
    public void writeFile() {
            FileWriter fw = null;
        try {
            fw = new FileWriter("C:\\cis\\cis2232\\RvkTestFile.txt", true);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(testString + System.lineSeparator());
            bw.close();
            fw.close();
            System.out.println("File writing successful.");
        } catch (IOException e) {
            System.out.println("Error!");
            e.printStackTrace();
        } finally {
            try {
                fw.close();
            } catch (IOException ex){
                System.out.println("Error closing file");
            }
        }
    }

    @Override
    public void run() {
        createFile();
        writeFile();
    }

}
