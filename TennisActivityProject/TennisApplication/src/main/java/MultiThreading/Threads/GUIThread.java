package MultiThreading.Threads;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import javax.swing.JOptionPane;

/**
 * Class to represent a GUI input form
 * @author Ryan van Kroonenburg
 * @since 20201202
 */
public class GUIThread extends Thread {

    String result;
    
    //create file if it does not exist.
    public void guiCreateFile() {

        while (!"exit".equals(result)) {
            result = JOptionPane.showInputDialog(null, "Enter text to write. Type 'exit' to exit.");
            try {
                File file = new File("C:\\cis\\cis2232\\RvkTestFileGUI.txt");
                if (file.createNewFile()) {
                    System.out.println("File created: " + file.getName());
                } else {
                    System.out.println("File already exists.");
                }
            } catch (IOException ex) {
                System.out.println("Error");
                ex.printStackTrace();
            }
        }
    }
    
    public void guiWriteFile() {
            FileWriter fw = null;
        try {
            fw = new FileWriter("C:\\cis\\cis2232\\RvkTestFileGUI.txt", true);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(result + System.lineSeparator());
            bw.close();
            fw.close();
            System.out.println("File writing successful.");
        } catch (IOException e) {
            System.out.println("Error!");
            e.printStackTrace();
        } finally {
            try {
                fw.close();
            } catch (IOException ex){
                System.out.println("Error closing file");
            }
        }
    }

    @Override
    public void run() {
        guiCreateFile();
        guiWriteFile();
    }

}
