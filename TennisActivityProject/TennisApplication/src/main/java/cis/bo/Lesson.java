package cis.bo;

/**
 * Class to represent a tennis lesson
 * 
 * @author Ryan van Kroonenburg
 * @since 20201130
 */
public class Lesson {

    public static final int MEMBER_PRIVATE = 55;
    public static final int MEMBER_2 = 30;
    public static final int MEMBER_3 = 21;
    public static final int MEMBER_4 = 16;
    public static final int NON_MEMBER_PRIVATE = 60;
    public static final int NON_MEMBER_2 = 33;
    public static final int NON_MEMBER_3 = 23;
    public static final int NON_MEMBER_4 = 18;

    private int rate;
    private String member;
    private int groupSize;
    private int numberOfHours;
    private int cost;
    
    //custom constructor
    public Lesson(int group, String mem, int numHours) {
        this.groupSize = group;
        this.member = mem;
        this.numberOfHours = numHours;
    }

    //default constructor
    public Lesson() {

    }

    public double calculateCost() {

        switch (member) {
            case "y":
                if (groupSize == 1) {
                    rate = MEMBER_PRIVATE;
                } else if (groupSize == 2) {
                    rate = MEMBER_2;
                } else if (groupSize == 3) {
                    rate = MEMBER_3;
                } else {
                    rate = MEMBER_4;
                }
                break;
            default:
                if (groupSize == 1) {
                    rate = NON_MEMBER_PRIVATE;
                } else if (groupSize == 2) {
                    rate = NON_MEMBER_2;
                } else if (groupSize == 3) {
                    rate = NON_MEMBER_3;
                } else {
                    rate = NON_MEMBER_4;
                }
                break;
        }

        cost = rate * numberOfHours;

        return cost;
    }
    
    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public String getMember() {
        return member;
    }

    public void setMember(String member) {
        this.member = member;
    }

    public int getGroupSize() {
        return groupSize;
    }

    public void setGroupSize(int groupSize) {
        this.groupSize = groupSize;
    }

    public int getNumberOfHours() {
        return numberOfHours;
    }

    public void setNumberOfHours(int numberOfHours) {
        this.numberOfHours = numberOfHours;
    }

    public int getCost() {
        return cost;
    }

}
