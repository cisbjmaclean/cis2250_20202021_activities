package cis.bo;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 * To Test the Lesson Class
 * 
 * @author Ryan van Kroonenburg
 * @since 20201130
 */
public class LessonJUnitTest {

    public LessonJUnitTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    @Test //1
    public void testCalculateCostPrivateMemberOneHour() {
        Lesson lesson = new Lesson(1, "y", 1);
        double cost = lesson.calculateCost();
        Assertions.assertEquals(55, cost);
    }

    @Test //2
    public void testCalculateCostTwoPeopleMemberOneHour() {
        Lesson lesson = new Lesson(2, "y", 1);
        double cost = lesson.calculateCost();
        Assertions.assertEquals(30, cost);
    }

    @Test //3
    public void testCalculateCostThreePeopleMemberOneHour() {
        Lesson lesson = new Lesson(3, "y", 1);
        double cost = lesson.calculateCost();
        Assertions.assertEquals(21, cost);
    }

    @Test //4
    public void testCalculateCostFourPeopleMemberOneHour() {
        Lesson lesson = new Lesson(4, "y", 1);
        double cost = lesson.calculateCost();
        Assertions.assertEquals(16, cost);
    }

    @Test //5
    public void testCalculateCostFourPeopleNonMemberOneHour() {
        Lesson lesson = new Lesson(4, "n", 1);
        double cost = lesson.calculateCost();
        Assertions.assertEquals(18, cost);
    }

    @Test //6
    public void testCalculateCostThreePeopleNonMemberThreeHours() {
        Lesson lesson = new Lesson(3, "n", 3);
        double cost = lesson.calculateCost();
        Assertions.assertEquals(69, cost);
    }

    @Test //7
    public void testCalculateCostTwoPeopleMemberTwoHours() {
        Lesson lesson = new Lesson(2, "y", 2);
        double cost = lesson.calculateCost();
        Assertions.assertEquals(60, cost);
    }

    @Test //8
    public void testCalculateCostFourPeopleNonMemberFourHours() {
        Lesson lesson = new Lesson(4, "n", 4);
        double cost = lesson.calculateCost();
        Assertions.assertEquals(72, cost);
    }

}
