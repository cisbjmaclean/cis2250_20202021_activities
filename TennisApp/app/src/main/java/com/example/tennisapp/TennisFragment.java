package com.example.tennisapp;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.tennisapp.bo.Lesson;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;

public class TennisFragment extends Fragment {

    private EditText editTextNumHours;
    private EditText editTextGroupSize;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
// Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_tennis, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Log.d("BJM", "The first fragment is being created");

        editTextNumHours = view.findViewById(R.id.editTextNumberOfHours);
        editTextGroupSize = view.findViewById(R.id.numGroupSize);
        CheckBox checkBox = (CheckBox) view.findViewById(R.id.chkMember);



        view.findViewById(R.id.buttonCalculateCost).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Lesson lesson = new Lesson(Integer.parseInt(editTextGroupSize.getText().toString()), checkBox.isChecked(), Integer.parseInt(editTextNumHours.getText().toString()));
                lesson.calculateCost();
                Log.d("BJM", "Clicked the calculate cost button");

                Log.d("BJM", "Value of number of hours is: "+editTextNumHours.getText().toString());

                Log.d("BJM", "Value of Group Size is: "+editTextGroupSize.getText().toString());

                boolean checkboxIsChecked = checkBox.isChecked();
                Log.d("BJM", "SECOND ATTEMPT ...Value of Member is: "+checkboxIsChecked);

                Log.d("BJM", "Value of Member is: "+checkBox.isChecked());

                Log.d("BJM", "Value of Lesson cost is: "+lesson.getCost());

                Snackbar.make(view, String.valueOf(lesson.getCost()), Snackbar.LENGTH_LONG).show();
            }

        });


// view.findViewById(R.id.button_first).setOnClickListener(new View.OnClickListener() {
// @Override
// public void onClick(View view) {
// NavHostFragment.findNavController(FirstFragment.this)
// .navigate(R.id.action_FirstFragment_to_SecondFragment);
// }
// });
    }
}